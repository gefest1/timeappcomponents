import 'package:flutter/material.dart';

class H1TextStyle {
  static const fontSize = 38.0;
  static const fontWeight = FontWeight.w700;
  static const height = 42 / 38;
}

class P1TextStyle {
  static const fontSize = 24.0;
  static const fontWeight = FontWeight.normal;
  static const height = 29 / 24;
}

class P2TextStyle {
  static const fontSize = 18.0;
  static const fontWeight = FontWeight.normal;
  static const height = 21 / 19;
}

class P3TextStyle {
  static const fontSize = 16.0;
  static const fontWeight = FontWeight.w400;
  static const height = 19 / 16;
}

class H2TextStyle {
  static const fontSize = 24.0;
  static const fontWeight = FontWeight.w700;
  static const height = 29 / 24;
}

class P4TextStyle {
  static const fontSize = 14.0;
  static const fontWeight = FontWeight.w400;
  static const height = 17 / 14;
}

class H3TextStyle {
  static const fontSize = 18.0;
  static const fontWeight = FontWeight.w700;
  static const height = 21 / 18;
}
