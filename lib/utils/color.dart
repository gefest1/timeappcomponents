import 'package:flutter/material.dart';

mixin ColorData {
  static const specsIcons = const Color(0xff61b3ff);
  static const specsMessagebackground = const Color(0xff61b3ff);
  static const specsButtonColorDefault = const Color(0xff007aff);
  static const specsButtonColorPressed = const Color(0xff005abd);
  static const specsStrokeActive = const Color(0xff0f84f4);
  static const specsInnerSucces = const Color(0xfff9fbff);
  static const illustrationsSkinColor = const Color(0xffeb7962);
  static const clientsIcons = const Color(0xff25b900);
  static const clientsInnerSucces = const Color(0xfff9fff9);
  static const clientsStrokeSucces = const Color(0xff0ef425);
  static const clientsButtonColorPressed = const Color(0xff00ab28);
  static const clientsButtonColorDefault = const Color(0xff00d62c);
  static const allMessagesbackground = const Color(0xfff8f8f8);
  static const allTextInactive = const Color(0xffcccccc);
  static const allInnerInactive = const Color(0xfff0f0f0);
  static const allInnerDefault = const Color(0xfffafafa);
  static const allInnerError = const Color(0xfffff9f9);
  static const allButtonsError = const Color(0xfff40e0e);
  static const allButtonsDefault = const Color(0xffc8c8c8);
  static const allMainBlack = const Color(0xff424242);
  static const allMainGray = const Color(0xffb7b7b7);
  static const allMainLightgray = const Color(0xfff8f8f8);
  static const allMainActivegray = const Color(0xff969696);
  static const allMainCaution = const Color(0xffff7575);
  static const mainGraybackground = const Color(0xfff8f8f8);
  static const grayScaleOffWhite = const Color(0xffFCFCFC);
}
