import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:time_app_components/time_app_components.dart';

class CustomTextField extends StatefulWidget {
  final FocusNode? focusNode;
  final String? label;
  final Widget? prefixIcon;
  final Color? labelColor;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final List<TextInputFormatter>? inputFormatters;
  final Function(String value)? onSubmitted;
  final BoxDecoration? enabledDecoration;
  final BoxDecoration? forcedDecoration;
  final EdgeInsets? innerPadding;

  const CustomTextField({
    this.onSubmitted,
    this.prefixIcon,
    this.focusNode,
    this.controller,
    this.label,
    this.keyboardType,
    this.inputFormatters,
    this.labelColor = const Color(0xff8E8E8E),
    this.forcedDecoration,
    this.innerPadding = const EdgeInsets.only(
      left: 20,
      right: 20,
      top: 6,
    ),
    this.enabledDecoration = const BoxDecoration(
      borderRadius: BorderRadius.all(Radius.circular(16)),
      color: Color(0xffFAFAFA),
      border: Border.symmetric(
        horizontal: BorderSide(
          width: 0,
          color: Colors.transparent,
        ),
        vertical: BorderSide(
          width: 0,
          color: Colors.transparent,
        ),
      ),
    ),
    Key? key,
  }) : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField>
    with SingleTickerProviderStateMixin {
  bool isChoosen = false;

  late AnimationController animationController = AnimationController(
    duration: Duration(milliseconds: 200),
    vsync: this,
  );

  late final anim = Tween(begin: 1, end: 0.71).animate(animationController);

  _check() {
    if (focusNode.hasFocus && !isChoosen) {
      animationController.forward();
      setState(() {
        isChoosen = true;
      });
    } else if (!focusNode.hasFocus && isChoosen && controller.text.isEmpty) {
      if (controller.text.isEmpty) {
        animationController.reverse();
      }
      setState(() {
        isChoosen = false;
      });
    }
  }

  late final TextEditingController controller =
      (widget.controller ?? TextEditingController())..addListener(_check);

  late final focusNode = (widget.focusNode ?? FocusNode())..addListener(_check);

  @override
  void initState() {
    super.initState();
    if (controller.text.isNotEmpty) animationController.forward();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 64,
      decoration: widget.forcedDecoration ??
          (isChoosen
              ? BoxDecoration(
                  color: textFieldMode == TextFieldEnum.spec
                      ? Color(0xffF9FBFF)
                      : Color(0xffF9FFFA),
                  borderRadius: BorderRadius.circular(16),
                  border: Border.all(
                    width: 0.5,
                    color: textFieldMode == TextFieldEnum.spec
                        ? Color(0xff0F84F4)
                        : Color(0xff0FF426),
                  ),
                )
              : widget.enabledDecoration),
      child: Row(
        children: [
          if (widget.prefixIcon != null) widget.prefixIcon!,
          Expanded(
            child: Padding(
              padding: widget.innerPadding!,
              child: Stack(
                children: [
                  TextField(
                    onSubmitted: widget.onSubmitted,
                    controller: controller,
                    keyboardType: widget.keyboardType,
                    focusNode: focusNode,
                    style: const TextStyle(
                      color: const Color(0xff323232),
                      fontSize: 18,
                      fontWeight: FontWeight.w400,
                    ),
                    decoration: InputDecoration(
                      contentPadding: EdgeInsets.only(top: 20),
                      border: InputBorder.none,
                    ),
                    inputFormatters: widget.inputFormatters,
                  ),
                  if (widget.label != null)
                    AnimatedBuilder(
                      animation: animationController,
                      builder: (_, child) {
                        return Transform(
                          transform: Matrix4.identity()
                            ..translate(
                              0.0,
                              -6 *
                                  Tween(begin: 0.6, end: 1.0)
                                      .evaluate(animationController),
                            )
                            ..scale(anim.value.toDouble()),
                          child: child,
                        );
                      },
                      child: IgnorePointer(
                        child: Align(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            widget.label!,
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w400,
                              color: widget.labelColor,
                            ),
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      ),
                    ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
