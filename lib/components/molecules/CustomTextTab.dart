import 'package:flutter/material.dart';

class CustomTextTab extends StatefulWidget {
  final String firstElement, secondElement;
  final String? thirdElement;
  final int lengthElements;
  final Color? color;
  final Color? colorBackground;

  const CustomTextTab({
    this.thirdElement,
    required this.firstElement,
    required this.secondElement,
    required this.lengthElements,
    required this.color,
    this.colorBackground,
    Key? key,
  }) : super(key: key);

  @override
  State<CustomTextTab> createState() => _CustomTextTabState();
}

class _CustomTextTabState extends State<CustomTextTab>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(length: widget.lengthElements, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return widget.lengthElements == 2
        ? Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            height: 71,
            color: widget.colorBackground == null
                ? Colors.transparent
                : widget.colorBackground,
            child: TabBar(
              controller: _tabController,
              labelColor: widget.color == null ? Colors.blue : widget.color,
              labelStyle: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
              unselectedLabelColor: Colors.black,
              unselectedLabelStyle: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
              indicator: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(
                  Radius.circular(15),
                ),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    spreadRadius: 0,
                    blurRadius: 5,
                    offset: const Offset(1, 1),
                  ),
                ],
              ),
              tabs: [
                Text(
                  widget.firstElement,
                ),
                Text(
                  widget.secondElement,
                ),
              ],
            ),
          )
        : Container(
            padding: const EdgeInsets.symmetric(
              horizontal: 10,
              vertical: 10,
            ),
            height: 71,
            color: Colors.grey[300],
            child: TabBar(
              controller: _tabController,
              labelColor: widget.color == null ? Colors.blue : widget.color,
              labelStyle: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
              unselectedLabelColor: Colors.black,
              unselectedLabelStyle: const TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w500,
              ),
              indicator: BoxDecoration(
                color: Colors.white,
                borderRadius: const BorderRadius.all(Radius.circular(15)),
                boxShadow: [
                  BoxShadow(
                    color: Colors.black.withOpacity(0.1),
                    spreadRadius: 0,
                    blurRadius: 5,
                    offset: const Offset(
                      1,
                      1,
                    ),
                  ),
                ],
              ),
              tabs: [
                Text(
                  widget.firstElement,
                ),
                Text(
                  widget.secondElement,
                ),
                Text(
                  widget.thirdElement!,
                )
              ],
            ),
          );
  }
}
