import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomListTile extends StatefulWidget {
  final String image, title;
  final void Function(bool) onChange;
  final bool initValue;
  final Color? activeColor;

  CustomListTile({
    required this.image,
    required this.title,
    required this.onChange,
    this.activeColor,
    this.initValue = false,
    Key? key,
  }) : super(key: key);

  @override
  _CustomListTileState createState() => _CustomListTileState();
}

class _CustomListTileState extends State<CustomListTile> {
  late bool _value = widget.initValue;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Row(
          children: [
            SizedBox(
              child: SvgPicture.asset(widget.image),
            ),
            const SizedBox(width: 10),
            Text(widget.title)
          ],
        ),
        SizedBox(
          height: 30,
          child: Switch.adaptive(
            value: _value,
            activeColor: widget.activeColor,
            onChanged: (value) {
              setState(() {
                _value = value;
              });
            },
          ),
        )
      ],
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
    );
  }
}
